const mongoose = require("mongoose");
const Genre = require("../models/genre");

exports.getAllGenres = (req, res) => {
    Genre.find()
        .then((docs) => {
            return res.status(200).json(docs);
        })
        .catch((err) => res.status(500).json(err));
};

exports.addGenre = (req, res) => {
    const genre = new Genre({
        genres: req.body.genres,
    });

    genre
        .save()
        .then(() =>
            res.status(201).json({ message: "Successfuly added" })
        )
        .catch((error) =>
            res.status(500).json({
                message: "Database error",
                error,
            })
        );
};