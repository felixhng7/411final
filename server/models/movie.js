const mongoose = require("mongoose");

const movieSchema = mongoose.Schema({
  image: { data: Buffer, contentType: String },
  title: { type: String, required: true },
  genre: { type: String, required: true },
  desc: { type: String, require: true },
});

module.exports = mongoose.model("movies", movieSchema, "movies");