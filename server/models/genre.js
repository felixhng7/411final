const mongoose = require("mongoose");

const genreSchema = mongoose.Schema({
    genres: { type: String, required: true },
});
const genre = mongoose.model("genres", genreSchema, "genres");

module.exports = genre;