const cors = require("cors");
const path = require("path");
const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const genreController = require("./controller/genre");
const movieController = require("./controller/movie");

const app = express();

app.use(bodyParser.urlencoded({ extended: true, limit: "10mb" }));
app.use(bodyParser.json({ limit: "10mb" }));

//To prevent CORS errors
app.use(cors());

//Connecting mongoDB
mongoose.connect("mongodb://localhost/Telecommunication", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

//Checking the connection to db
var db = mongoose.connection;
db.once("open", () => console.log("Connected to database"));
db.on("error", console.error.bind(console, "connection error:"));

app.use(express.static("./uploads"));

//routes

app.get("/", function (req, res) {
  res.send('nothing to see.');
});
app.get("/getgenres", genreController.getAllGenres);
app.post("/addgenre", genreController.addGenre);
app.get("/getmovies", movieController.getAllMovies);
app.post("/addmovie", movieController.addMovie);



const port = process.env.PORT || 5000;
console.log(port);
app.listen(port, () => console.log(`Server running on port ${port}`));
module.exports = app;