# 411 final project

![Alt text](demo.PNG "preview image")

This project is a simple movie library app, it allows add, search and filter movies.

__FrontEnd__: React.JS, Redux, react-google-login
__Backend__: Node.JS, Express.JS
__Database__: MongoDB, Mongoose

### Features
- Sign in and logout with google account
- Add a new movie to the list
- Search and filter movies

### Installation 

- First install mongoDB and create a database named "Telecommunication" and collections named "genres" and "movies"

- Setup the project and install the packages by running in both server/client folder: 
```bash
npm install
```
 - Run servers with:
```bash
npm start
```

*__Don't forget to do it in both folders__*
 

