import React from "react";
import { connect } from "react-redux";

import { addMovie } from "../../flow/actions/moviesAction";
import { getGenres } from "../../flow/actions/genreAction";

import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import Select from 'react-select';

class AddMovieForm extends React.Component {
  state = {
    data: {
      title: "",
      genre: "",
      desc: "",
      image: null,
    },
    genres: [],
    errors: {},
  };

  componentDidMount() {
    this.props.getGenres();
  }

  handleChange = ({ currentTarget: input }) => {
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data });
  };

  handleGenre = (e) => {
    const data = { ...this.state.data };
    data["genre"] = e.value;
    this.setState({ data });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.addMovie(this.state.data);
  };

  uploadImage = (e) => {
    if (e.target.files[0]) {
      const data = { ...this.state.data };
      data.image = e.target.files[0];
      this.setState({ data });
    }
  };

  render() {
    const { errors, data } = this.state;
    const { title, genre } = data;
    const { genres } = this.props;

    let selectGenre = genres.map((g) => ({ value: g.genres, label: g.genres }));
    return (
      <Container>
        <Form>
          <FormGroup>
            <Label for="title">Title</Label>
            <Input
              name="title"
              value={title}
              label="Title"
              onChange={this.handleChange}
              placeholder="Enter the title..."
              error={errors["title"]}
              autoFocus
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">Genre</Label>
            <Select
              name="genre"
              label="Genre"
              onChange={this.handleGenre}
              value={genre}
              options={selectGenre}
            />
          </FormGroup>
          <FormGroup>
            <Label for="image">Cover</Label>
            <Input
              name="image"
              label="Image"
              onChange={this.uploadImage}
              error={errors["coverImage"]}
              accept="image/*"
              type="file"
            />
          </FormGroup>
          <FormGroup>
            <Label for="desc">Description</Label>
            <Input
              name="desc"
              label="Desc"
              placeholder="Movie description..."
              onChange={this.handleChange}
              error={errors["desc"]}
              type="textarea"
            />
          </FormGroup>
          <Button onClick={this.handleSubmit}>Submit</Button>
        </Form>
      </Container>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    addMovie: (movie) => dispatch(addMovie(movie)),
    getGenres: () => dispatch(getGenres()),
  };
};

const mapStateToProps = (state) => {
  return {
    genres: state.genre.genres,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddMovieForm);

