import Axios from "axios";
import { GET_MOVIES_SUCCESS, GET_MOVIES_ERROR } from "./actionTypes";

const instance = Axios.create({baseURL: 'http://localhost:5000'})


export const getMovies = () => {
  return async (dispatch) => {
    try {
      const result = await instance.get("/getmovies");
      dispatch({ type: GET_MOVIES_SUCCESS, payload: result.data.movies });
    } catch (error) {
      dispatch({ type: GET_MOVIES_ERROR, error });
    }
  };
};

export const addMovie = (movie) => {
  const contentType = {
    headers: {
      "content-type": "multipart/form-data",
    },
  };
  let formData = new FormData();
  formData.append("title", movie.title);
  formData.append("desc", movie.desc);
  formData.append("genre", movie.genre);
  formData.append("image", movie.image);

  return async (dispatch) => {
    try {
      const result = await instance.post(
        "/addmovie",
        formData,
        contentType
      );
      dispatch({ type: GET_MOVIES_SUCCESS, payload: result.data.movies });
    } catch (error) {
      dispatch({ type: GET_MOVIES_ERROR, error });
    }
  };
};

