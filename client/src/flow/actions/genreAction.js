import { GET_GENRES_ERROR, GET_GENRES_SUCCESS } from "./actionTypes";
import Axios from "axios";

const instance = Axios.create({baseURL: 'http://localhost:5000'})

export const getGenres = () => {
  return async (dispatch) => {
    try {
      const result = await instance.get("/getgenres");
      dispatch({ type: GET_GENRES_SUCCESS, payload: result.data });
    } catch (error) {
      dispatch({ type: GET_GENRES_ERROR, error });
    }
  };
};

