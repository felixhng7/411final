import {
  LOGIN_SUCCESS,
  SIGNOUT,
} from "./actionTypes";

export const signIn = (userData) => {
  return async (dispatch) => {
    dispatch({ type: LOGIN_SUCCESS, payload: userData });
    localStorage.setItem('logged', userData.googleId);
  };
};

export const logged = (userData)=>{
  return async (dispatch) => {
    dispatch({ type: LOGIN_SUCCESS, payload: userData });
    localStorage.setItem('logged', userData);
  };
}

export const signOut = () => {
  return (dispatch) => {
    dispatch({ type: SIGNOUT });
    localStorage.removeItem('logged');
  };
};
