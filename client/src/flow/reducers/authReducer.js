import {
  LOGIN_SUCCESS,
  SIGNOUT,
} from "../actions/actionTypes";

const initState = {
  loggedIn: false,
  userData: {},
};
export default function red (state = initState, action) {
  
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        loggedIn: true,
        userData: action.payload,
      };

    case SIGNOUT:
      return {
        ...state,
        userData: {},
        loggedIn: false,
      };
    default:
      return state;
  }
}
