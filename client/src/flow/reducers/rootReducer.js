import { combineReducers } from "redux";
import movieReducer from "./movieReducer";
import genreReducer from "./genreReducer";
import authReducer from "./authReducer";

export default combineReducers({
  movie: movieReducer,
  genre: genreReducer,
  auth: authReducer
});
