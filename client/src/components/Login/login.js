
import React, { Component } from 'react';
import GoogleLogin from 'react-google-login'
import { connect } from "react-redux";

import { signIn } from "../../flow/actions/authAction";

class login extends Component {

    responseGoogle = (response) => {
        console.log(response);
        console.log(response.profileObj);
    }
    responseGoogleSuccess = (response) => {
        this.props.signIn(response.profileObj)
    }

    render() {
        return (
            <div>
                <GoogleLogin
                    clientId="101471783814-3i9c7crctghoe2kjc52v6obgpvrgg8ka.apps.googleusercontent.com"
                    buttonText="Login"
                    onSuccess={this.responseGoogleSuccess}
                    onFailure={this.responseGoogle}
                    cookiePolicy={'single_host_origin'}
                />
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    signIn: (userData) => {
        dispatch(signIn(userData));
    },
});


export default connect(null, mapDispatchToProps)(login);