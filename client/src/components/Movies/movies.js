import React, { Component } from "react";
import { connect } from "react-redux";
import { MoviesTable } from "..";

import { Input } from 'reactstrap';
import Select from 'react-select';

import { getMovies } from "../../flow/actions/moviesAction";
import { getGenres } from "../../flow/actions/genreAction";

class Movies extends Component {
  state = {
    genres: [],
    pageSize: 12,
    currentPage: 1,
    currentGenre: "All",
    searchFilter: "",
    rating: 0,
  };

  componentDidMount() {
    this.props.getMovies();
    this.props.getGenres();
  }

  handleChange = (name, value) => {
    this.setState({ [name]: value, currentPage: 1 });
  };

  handleGenre = (e) => {
    this.setState( {currentGenre:e.value})
  }

  onPageChange = (page) => {
    this.setState({ currentPage: page });
  };

  render() {
    const {
      currentGenre,
      searchFilter,
      rating,
    } = this.state;
    const { movies, genres, loggedIn } = this.props;


    let filteredMovies = [];

    function filterRating(items, rating) {
      if (rating === 0) return items;
      return items.filter((item) => item.rate >= rating);
    }
    
    function categorization (allMovies, genre) {
      if (genre === "All") return allMovies;
      else return allMovies.filter((movie) => movie.genre === genre);
    }

    function search (items, filter, filterBy) {
      if (!items) return null;
      if (
        !filter ||
        !filterBy ||
        !items[0].hasOwnProperty(filterBy) ||
        !filter.trim()
      )
        return items;
      return items.filter((element) =>
        element[filterBy]
          .toString()
          .toLowerCase()
          .startsWith(filter.toString().toLowerCase())
      );
    }
    
    filteredMovies = search(movies, searchFilter, "title");
    filteredMovies = categorization(filteredMovies, currentGenre);
    filteredMovies = filterRating(filteredMovies, rating);
    
    let selectGenre = genres.map((g) => ({ value: g.genres, label: g.genres }));

    return (
      <div className="background-container">
        <div className="mx-5 py-5">
          <div className="row">
            <div className="col-lg-2 col-sm-12">
              <h4 className="text-muted text-left p-1">Genres</h4>

              <Select
              name="genre"
              label="Genre"
              onChange={this.handleGenre}
              options={selectGenre}
            />
            </div>

            <div className="col-lg-10 col-sm-12">
              <Input
                onChange={(event) =>
                  this.handleChange("searchFilter", event.target.value)
                }
                className = "wd-5"
                label="Search Movie"
                placeholder="Search..."
              />
              <br />
              {!!filteredMovies ? (
                <MoviesTable
                  pageSize={12}
                  currentPage={1}
                  movies={filteredMovies}
                />
              ) : (
                <h1 className="text-white">No Movies</h1>
              )}
              <br />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    movies: state.movie.movies,
    genres: state.genre.genres,
    loggedIn: state.auth.loggedIn,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMovies: () => dispatch(getMovies()),
    getGenres: () => dispatch(getGenres()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Movies);
