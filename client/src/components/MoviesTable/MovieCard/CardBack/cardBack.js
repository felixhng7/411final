import React from 'react'
import './style.css';

const CardBack = ({ ToggleFavouriteCard, id, liked, desc }) => {
  return (
    <div className="back">
      <h5> Summary </h5>
      <p> {desc} </p>
    </div>
  );
}

export default CardBack;
