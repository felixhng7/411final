import React from "react";
import "./style.css";

const CardFront = ({
  coverImage,
  title,
  genre,
}) => {
  return (
    <div className="front">
      <img src={coverImage} alt="coverImage" />
      <div className="card-footer">
        <h4> {title} </h4>
        <p>
          {genre}
        </p>
      </div>
    </div>
  );
};
export default CardFront;

