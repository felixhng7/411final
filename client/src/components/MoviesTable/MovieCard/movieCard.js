import React from "react";

import FlippingCardFront from "./CardFront/cardFront";
import FlippingCardBack from "./CardBack/cardBack";
import "./style.css";

export default function table({ movie }) {
  const {
    _id,
    title,
    genre,
    image,
    desc
  } = movie;

  const encodedImage = new Buffer(image.data, "binary").toString("base64");
  const coverImage = "data:image/jpeg;base64," + encodedImage;

  function flipCard(cardID) {
    const card = document.getElementById(`${cardID}`);
    card.classList.toggle("flipped");
  }

  return (
    <div className="card-container">
      <div className="card-wrapper" id={_id} onClick={() => flipCard(_id)}>
        <FlippingCardFront
          coverImage={coverImage}
          genre={genre}
          title={title}
        />

        <FlippingCardBack desc={desc} />
      </div>
    </div>
  );
}

