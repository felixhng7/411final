import React, { Component } from "react";
import {
  Route,
  Switch,
  BrowserRouter as Router,
} from "react-router-dom";

import { connect } from "react-redux";

import Movies from "./components/Movies/movies";
import AddMovieForm from "./pages/AddMovie/addMovie";
import Login from "./components/Login/login";
import Navbar from "./components/Navbar/navBar";

import store from "./flow/store";
import { logged } from "./flow/actions/authAction";

import "./App.css";

class App extends Component {

  componentDidMount() {
    if (localStorage.getItem('logged')){
      store.dispatch(logged(localStorage.getItem('logged')));}
  }

  render() {

    const { loggedIn } = this.props;

    

    return (

      <Router>
        <div className="App">


          <Switch>
            {loggedIn === false ?
              <div>

                <h1>Login first</h1>
                <Login />
              </div>
              :
              <div>
                <Navbar />
                <Route path="/addnew" component={AddMovieForm} />
                <Route exact path="/" component={Movies} />
              </div>}
          </Switch>

        </div>
      </Router>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    movies: state.movie.movies,
    genres: state.genre.genres,
    loggedIn: state.auth.loggedIn,
  };
};

export default connect(mapStateToProps)(App);